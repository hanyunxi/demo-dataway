package com.hy.config;

import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.dataql.DimUdf;
import net.hasor.dataql.DimUdfSource;
import net.hasor.dataql.QueryApiBinder;
import net.hasor.db.JdbcModule;
import net.hasor.db.Level;
import net.hasor.spring.SpringModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;


@DimModule
@Component
public class DataWayModule implements SpringModule {
    @Autowired private DataSource dataSource = null;

    @Override
    public void loadModule(ApiBinder apiBinder) throws Throwable {

        // .DataSource form Spring boot into Hasor
        apiBinder.installModule(new JdbcModule(Level.Full, this.dataSource));

        // .custom DataQL
        QueryApiBinder queryBinder = apiBinder.tryCast(QueryApiBinder.class);
        queryBinder.loadUdf(
                apiBinder.findClass(DimUdf.class), aClass -> true, springTypeSupplier(apiBinder));
        queryBinder.loadUdfSource(
                apiBinder.findClass(DimUdfSource.class), aClass -> true, springTypeSupplier(apiBinder));
    }
}
